var storedJson=[];
var currentPage = -1;
var app = angular.module("topgit",['ui.router']);
  app.config(['$stateProvider','$urlRouterProvider',function($stateProvider,$urlRouterProvider) {
    $stateProvider
      .state('home',{
        url:'/',
        templateUrl:'./templates/home.html',
        controller:'homeCtrl'
      })
      .state('profile',{
        url:'/search/profile?name',
        templateUrl:'./templates/profile.html',
        controller:'profileController',
        controllerAs:'profileCtrl'
      })
      .state('search',{
        url:'/search?name',
        templateUrl:'./templates/search.html',
        controller:'searchController',
        controllerAs:'searchCtrl'
      });
      $urlRouterProvider.otherwise('/');
  }]);
