app.controller('searchController',[
  '$scope',
  '$http',
  'httpService',
  'pagination',
  '$state',
  'profileService',
  function($scope,$http,httpService,pagination,$state,profileService) {
    var lang = $scope.mctrl.searchtext;
    this.seekValue = 0;
    console.log("search page:" + lang);
    this.gitArray = [];
    var thisRef = this;
    httpService.setSearckKey(lang);
    httpService.getData(1,function(data) {
        $scope.mctrl.totalRepo = data.total_count;
        storedJson=[];
        currentPage=-1;
        storedJson.push(data.items);
        thisRef.gitArray = data.items;
        currentPage++;
        console.log("current Page: "+(currentPage+1));
        console.log("total Page: "+storedJson.length);
    });
    this.pageFirst = function() {
      pagination.pageFirst(thisRef);
    }
    this.pageNext = function() {
        pagination.pageNext(thisRef);
    }
    this.pagePrev = function() {
        pagination.pagePrev(thisRef);
    }
    this.goToGitHub = function(row) {
      profileService.setRepoObject(row);
      $state.go('profile',{name:row.full_name});
    }
  }
]);
