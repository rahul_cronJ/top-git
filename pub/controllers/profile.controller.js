app.controller('profileController',[
  'profileService',
  function(profileService) {

      this.repo = profileService.getRepoName();
      this.author = profileService.getAuthor();
      this.gitHub = profileService.getGitLink();
      this.repoLink = profileService.getRepoLink();
      this.lang = profileService.getLang();
      this.desc = profileService.getDesc();
      this.stars = profileService.getStars();
      this.score = profileService.getScore();
      this.imageLink = profileService.getImage();
  }
]);
