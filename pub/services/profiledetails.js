app.service("profileService",function($http) {
    var repoDetails={};
    this.setRepoObject = function(repoObj) {
      repoDetails = repoObj;
    }
    this.getRepoName = function() {
      return repoDetails.full_name;
    }
    this.getAuthor = function() {
      return repoDetails.owner.login;
    }
    this.getGitLink = function() {
      return repoDetails.owner.html_url;
    }
    this.getRepoLink = function() {
      return repoDetails.html_url;
    }
    this.getLang = function() {
      return repoDetails.language;
    }
    this.getDesc = function() {
      return repoDetails.description;
    }
    this.getStars = function() {
      return repoDetails.stargazers_count;
    }
    this.getScore = function() {
      return repoDetails.score;
    }
    this.getImage = function() {
      return repoDetails.owner.avatar_url;
    }
});
