app.service("httpService",function($http) {
  var searchKey="";
  var page=0;
  var totalPages=-1;
  this.setSearckKey = function(lang) {
    searchKey = lang;
  }

  this.getData = function(page,callBack) {
    var gitUrl = "https://api.github.com/search/repositories?q="+searchKey+"&page="+page+"&per_page=10";
    $http({method:'GET',url:gitUrl})
      .success(function(data, status, headers, config) {
        if(data.items.length>0){
          totalPages++;
          callBack(data);
        }
      })
      .error(function(data, status, headers, config) {
        console.log("error in getting data from github");
      });
    }
});
