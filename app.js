var express = require('express');
var app = express();

app.use(express.static('pub'));

app.get('/',function(req,res) {
    res.sendFile(__dir + "/pub/index.html");
});

app.listen(3000,function() {
  console.log("server started...");
});
